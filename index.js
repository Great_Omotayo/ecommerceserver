const express = require('express');
const cors = require('cors');
const bodyparser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');
const config = require('./config/database');
const passport =  require('passport');
const app = express();
app.use(fileUpload());
mongoose.connect(config.database);
 mongoose.connection.on("connected", function(){
    console.log("Connection Estabilished")
});
console.log("Connection Estabilished");
app.get("/into", function(req,res){
	res.send("okay");
});
app.get("/love", function(req,res){
	res.send("love");
});
app.use(bodyparser.json());
app.use(passport.initialize());
app.use(passport.session());
app.use(cors());
require('./config/passport');
app.use(express.static(path.join(__dirname, 'dist')));
const routes = require('./routes');
app.use('/',routes);
app.listen(process.env.PORT || 8080);
console.log(__dirname);