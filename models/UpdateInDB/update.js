const mongoose = require("mongoose");
var order = require("../orders");
var recommend = require("../recommend");
var trending = require("../trending");
var product = require("../product");
var payment = require("../payment");
var popular = require("../popular");
const add = require("../addtoDB/add.js");
var handle = require('../../handle_images');
var user = require('../user');

module.exports.updateProduct = function(body,img, callback){
  product.findById({'_id':body._id},function(err,product){
    if(err) callback(err,null);
    else {
      product.Name = body.Name || product.Name;
      product.details = body.details || product.details;
      product.features = body.features || product.features;
      product.categories = body.categories || product.categories;
      product.Quantity = body.Quantity || product.Quantity;
      product.price = body.price || product.price;
      product.size = body.size || product.size;
      product.images = body.images || product.images;
      var savedimg;
      //console.log(img);
      if(img != null){
          if(img.photo instanceof Array){
            savedimg = handle.updateMultiImage(img.photo, body.images);
          }else{
            savedimg = handle.updateSingleImage(img.photo, body.images);
        }  
        console.log(savedimg);
        product.images = savedimg;
      }
      product.save(callback);
    }

    });
}
module.exports.userdata = function(body,callback){
  console.log(body.email);
  user.findOne({'CustomerEmail':body.email},function(err,user){
    if(err) callback(err,null);
    else{
      console.log(user);
      user.CustomerPhone = body.phone || user.phone;
      user.CustomerAddress = body.address || user.address;
      user.save(callback);
    }
  });
}
module.exports.seenOrder = function(id,callback){
  var criteria = {order_ID:{$in:id}};
  order.update(criteria,{seen:"1"},{multi:true},callback);
}
module.exports.completedOrder = function(id,callback){
  var criteria = {order_ID:{$in:id}};
  order.update(criteria,{completed:"1"},{multi:true},callback);
}
