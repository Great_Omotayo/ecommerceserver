var mongoose = require("mongoose");
const paymentschema  = mongoose.Schema({
	product_ID:{
		type:String,
		require:true
	},
	order_ID:{
		type:String
	},
	meansOfPayment:{
		type:String,
		require:true
	},
	PaidBy:{
		type:String,
		require:true
	},
	Amount:{
		type:String,
		require:true
	},
	create_date:{
		type:Date,
		default:Date.now
	}
});
var payment = module.exports = mongoose.model("payment",paymentschema);