var mongoose = require("mongoose");
const trendingschema  = mongoose.Schema({
	Name:{
		type:String,
		require:true
	},
	details:{
		type:String
	},
	features:{
		type:String,
		require:true
	},
	categories:{
		type:String,
		require:true
	},
	Quantity:{
		type:Number,
		require:true
	},
	price:{
		type:Number,
		require:true
	},
	size:[],
	images:[],
	create_date:{
		type:Date,
		default:Date.now
	}
});
var trendingproduct = module.exports = mongoose.model("trendingproducts",trendingschema);