var mongoose = require("mongoose");
const Userschema  = mongoose.Schema({
	CustomerName:{
		type:String
	},
	CustomerPhone:{
		type:String
	},
	CustomerEmail:{
		type:String,
		require:true
	},
	password:{
		type:String,
		require:true
	},
	CustomerAddress:{
		type:String
	},
	create_date:{
		type:Date,
		default:Date.now
	}
});
var user = module.exports = mongoose.model("Users",Userschema);