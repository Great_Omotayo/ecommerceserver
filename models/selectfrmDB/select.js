const mongoose = require("mongoose");
var order = require("../orders");
var recommend = require("../recommend");
var trending = require("../trending");
var product = require("../product");
var payment = require("../payment");
var popular = require("../popular");
var pick = require("../pick");
var user = require('../user');
var bcrypt = require('bcryptjs');

module.exports.allProduct = function(callback){
	product.find(callback);
}
module.exports.bought = function(id,callback){
	order.find({CustomerID: id},callback);
}
module.exports.findUser = function(email, callback){
	user.findOne({CustomerEmail:email},callback);
}
module.exports.findOneUser = function(id, callback){
	user.findOne({_id:id},callback);
}
module.exports.getstockbysearch = function(value,callback){
	console.log(value);
	product.find(value,callback);
	
}
module.exports.OutOfStock = function(callback){
	product.find({Quantity: 0}, callback);
}
module.exports.UnOpenedOrders = function(callback){
	order.find({seen: "0"}).sort([['create_date', -1]]).exec(callback);
}
module.exports.orders = function(callback){
	order.find().sort([['create_date', -1]]).exec(callback);
}
module.exports.recommend = function(callback){
	recommend.find(callback);
}
module.exports.trending = function(callback){
	trending.find(callback);
}
module.exports.popular = function(callback){
	popular.find(callback);
}
module.exports.pickOfTheDay = function(callback){
	pick.find(callback);
}
module.exports.completedOrders = function(callback){
	order.find({completed: "1"}).sort([['create_date', -1]]).exec(callback);
}
module.exports.user = function(callback){
	user.find(callback);
}