var mongoose = require("mongoose");
const Orderschema  = mongoose.Schema({
	product_ID:{
		type:String,
		require:true
	},
	modeOfpayment:{
		type:String,
		require:true
	},
	order_ID:{
		type:String
	},
	seen:{
		type:String
	},
	completed:{
		type:String
	},
	Quantity:{
		type:Number,
		require:true
	},
	CustomerID:{
		type:String,
		require:true
	},
	Amount:{
		type:Number,
		require:true
	},
	create_date:{
		type:Date,
		default:Date.now
	}
});
var order = module.exports = mongoose.model("Orders",Orderschema);