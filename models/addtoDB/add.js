const mongoose = require("mongoose");
var order = require("../orders");
var recommend = require("../recommend");
var trending = require("../trending");
var product = require("../product");
var payment = require("../payment");
var popular = require("../popular");
var handle = require('../../handle_images');
var pick = require("../pick");
var user = require('../user');
var bcrypt = require('bcryptjs');

module.exports.addProduct = function(body,img,callback){
	var savedimg;
	console.log(img);
	if(img != null){
      if(img.photo instanceof Array){
		savedimg = handle.uploadImage(img.photo);
      }else{
		savedimg = handle.singleuploadImage(img.photo);
	  }  
	  body.images.push.apply(body.images, savedimg);
	}
    console.log(body);
	newproduct = new product(body);
	newproduct.save(callback);
}
module.exports.createAccount = function(body,callback){
	bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(body.password, salt, function(err, hash) {  
		body.password = hash;
        newuser = new user(body);
        newuser.save(callback);
    });
});
    
}
module.exports.addrecommend = function(body,callback){
	recommend.insertMany(body,callback);
}
module.exports.addtrending = function(body, callback){
	trending.insertMany(body,callback);
}
module.exports.addpopular = function(body, callback){
	popular.insertMany(body,callback);
}
module.exports.order = function(body, callback){
	order.insertMany(body,callback);
}
module.exports.addpicked = function(body, callback){
	pick.insertMany(body,callback);
}