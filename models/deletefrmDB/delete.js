const mongoose = require("mongoose");
var order = require("../orders");
var recommend = require("../recommend");
var trending = require("../trending");
var product = require("../product");
var payment = require("../payment");
var popular = require("../popular");

module.exports.delete = function(_id,callback){
  product.findByIdAndRemove(_id, callback);
}
module.exports.deletepopular = function(id,callback){
	popular.findByIdAndRemove(id,callback);
}
module.exports.deletetrending = function(id, callback){
	trending.findByIdAndRemove(id,callback);
}
module.exports.deleterecommend = function(id,callback){
	recommend.findByIdAndRemove(id,callback);
}