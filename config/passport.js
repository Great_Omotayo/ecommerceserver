var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,
    config = require('../config/database'),
    select = require('../models/selectfrmDB/select.js');
var passport =  require('passport');
var bcrypt = require('bcryptjs');
var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.secret;
console.log(config.secret);
passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
  console.log(jwt_payload);
    var body = {email:jwt_payload.email,password:jwt_payload.password};
    select.ConfirmAttr(body,function(err,status){
      if(err){
       return done(err, false);
      }  
      if(status===true){
       return done(null, status);
      }
      else
        return done(null, false);
      });
  
}));