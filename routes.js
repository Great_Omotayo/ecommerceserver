var express = require('express');
const path = require('path');
var router = express.Router();
const add = require("./models/addtoDB/add.js");
const remove = require("./models/deletefrmDB/delete.js");
const update = require("./models/UpdateInDB/update.js");
const select = require("./models/selectfrmDB/select.js");
var bcrypt = require('bcryptjs');
var config = require('./config/database');
var passport = require('passport');
var jwt = require('jsonwebtoken');

router.post("/addproduct", function(req,res){
 var body = JSON.parse(req.body.newproduct);
 add.addProduct(body,req.files, function(err, newproduct){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':newproduct});
 });
});
router.post("/stockbySearch", function(req,res){
  console.log(req.body.search);
 select.getstockbysearch(req.body.search, function(err, product){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':product});
     });
 });
router.post("/updateproduct", function(req,res){
 var body = JSON.parse(req.body.updateproduct);
 update.updateProduct(body,req.files, function(err, updatedproduct){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':updatedproduct});
 });
});
router.post("/seenAndGet", function(req,res){
  console.log(req.body.id);
 update.seenOrder(req.body.id,function(err, order){
   if(err) res.json({'status':'failed'});
   else {
      select.findOneUser(req.body.customerID, function(err,user){
      console.log(user);
      res.json({'status':'success','product':order, 'customer':user});
    });
    }
 });
});
router.post("/completed", function(req,res){
 update.completedOrder(req.body.id,function(err, order){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':order});
 });
});
router.post("/recommend", function(req,res){
  add.addrecommend(req.body.addrecommend, function(err, recommend){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':recommend});
 });
});
router.get("/getRecommend", function(req,res){
  select.recommend(function(err, recommend){
   if(err) res.json({'status':'failed'});
   else {
    console.log(recommend);
    res.json({'status':'success','product':recommend});
  }
});
});
router.post("/picked", function(req,res){
  add.addpicked(req.body.addpicked, function(err, picked){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':picked});
 });
});
router.get("/getPickOfTheDay", function(req,res){
  select.pickOfTheDay(function(err, pick){
   if(err) res.json({'status':'failed'});
   else {
        //console.log(recommend);
        res.json({'status':'success','product':pick});
      }
    });
});
router.post("/trending", function(req,res){
  add.addtrending(req.body.addtrending, function(err, trending){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':trending});
 });
});
router.get("/gettrending", function(req,res){
  select.trending(function(err, trending){
   if(err) res.json({'status':'failed'});
   else {
    res.json({'status':'success','product':trending});
  }
});
});
router.post("/addpopular", function(req,res){
	add.addpopular(req.body.addpopular, function(err, popular){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success','product':popular});
 });
});
router.get("/getPopular", function(req,res){
  select.popular(function(err, popular){
   if(err) res.json({'status':'failed'});
   else {
    res.json({'status':'success','product':popular});
  }
});
});
router.get("/showdata", function(req,res){
  select.pickOfTheDay(function(err, pick){
   if(err) res.json({'status':'failed'});
   else {
        select.trending(function(err, trending){
        if(err) res.json({'status':'failed'});
        else {
             select.popular(function(err, popular){
             if(err) res.json({'status':'failed'});
             else {
              res.json({'status':'success','mostpopular':popular,'trending':trending,'daypick':pick});
            }
          });
        }
        });
      }
    });
});
router.post("/orders", function(req,res){
	add.order(req.body.orders, function(err, orders){
   if(err) res.json({'status':'failed'});
   else {
    update.userdata(req.body.userdata,function(err,updatedata){
     if(err) "";
     else res.json({'status':'success','product':orders, 'user':updatedata});
    });
  }
 });
});
router.post("/updateproduct/:id", function(req,res){
 remove.updateproduct(req.params.id,function(err,updateproduct){
  if(err) res.json({'status':'failed'});
  else res.json({'status':'success'});
});
});
router.post("/userbought", function(req,res){
  console.log(req.body._id);
 select.bought(req.body._id,function(err,bought){
  if(err) res.json({'status':'failed'});
  else {
    res.json({'status':'success','bought':bought});
  }
});
});
router.get("/allproduct",function(req,res){
 select.allProduct(function(err,product){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success',"product":product});
 });
});
router.get("/recommend",function(req,res){
 select.recommend(function(err,product){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success',"product":product});
 });
});
router.get("/popular",function(req,res){
 select.popular(function(err,product){
   if(err) res.json({'status':'failed'});
   else res.json({'status':'success',"product":product});
 });
});
router.post("/createAccount",function(req,res){
  add.createAccount(req.body,function(err,newuser){
    if(err) res.json({"status":'failed'});
    else res.json({"status":"success",'user':newuser});
  })
});
router.post("/loggIn", function(req,res){
 let email = req.body.email; 
 console.log(email);
 select.findUser(email, function(err,user){
   if(err) res.json({'status':'failed'});
   else if(!user) res.json({'status':'failed','message': 'No Account found, Please Sign Up'});
   else{
    bcrypt.compare(req.body.password, user.password,function(err,status){
     if(status === false) res.json({'status':'failed','message': 'Email does not match password'});
     else{
      secret = config.secret;
      var payload = {useremail:user.email,password:req.body.password};
          const token = jwt.sign(payload,secret,{expiresIn: 64800});
          response = {success:true, token: 'JWT ' +token,  result:
              {
                  id: user._id,
                  email:user.CustomerEmail,
                  phone:user.CustomerPhone == undefined ? "":user.CustomerPhone,
                  Name:user.CustomerName == undefined ? "":user.CustomerName,
                  address:user.CustomerAddress == undefined ? "":user.CustomerAddress
              }
          }
          console.log(response);
      res.json({status:"success",response:response});
     }
     });
   }
 });
});
router.get('/admin', function (req, res) {
   select.allProduct(function(err,product){
    if(err) res.json({'status':'failed'});
    else{
      select.OutOfStock(function(err,stockproduct){
       if(err) res.json({'status':'failed'});
       else {
          select.UnOpenedOrders(function(err,unopenedproduct){
            if(err) res.json({'status':'failed'});
            else {
                select.orders(function(err,orderproduct){
                 if(err) res.json({'status':'failed'});
                 else {
                  select.completedOrders(function(err,completedOrders){
                    if(err) res.json({'status':'failed'});
                    else res.json({"status":'success',"completedOrders":completedOrders,"product": product, "OutOfStock":stockproduct, "unopenedproduct":unopenedproduct,"orderproduct":orderproduct})
                  });
                }
               });
              }
            });
          }
        });
      }
    }); 
});
router.get("/deletepopular",function(req,res){
  req.body.deletepopular.forEach(function(popular){
    remove.deletepopular(popular,function(err,deleted){});
  });
});
router.get("/deletetrending",function(req,res){
  req.body.deletetrending.forEach(function(trending){
    remove.deletetrending(trending,function(err,deleted){});
  });
});
router.get("/deleterecommended",function(req,res){
  req.body.deleterecommended.forEach(function(recommended){
    remove.deleterecommend(recommended,function(err,deleted){});
  });
});
router.get("/user",function(req,res){
 select.user(function(err,data){
   console.log(data);
 });
});
router.get("/*", function(req, res) {
   //res.send("am here..");
   res.sendFile(path.join(__dirname + '/dist/index.html'));
 });
  module.exports = router;